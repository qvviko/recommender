let changeColor = document.getElementById('query_articles');
let bkg = chrome.extension.getBackgroundPage();

function getSelectionText() {
    var text = "";
    if (window.getSelection) {
        text = window.getSelection().toString();
    } else if (document.selection && document.selection.type !== "Control") {
        text = document.selection.createRange().text;
    }
    return text;
}


changeColor.onclick = function (element) {
    chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
        // base on this!!! https://developer.chrome.com/extensions/messaging
        chrome.tabs.executeScript({
            code: "window.getSelection().toString();"
        }, function (query) {
            if (query !== '') {
                chrome.runtime.sendMessage({'message': "REQ_articles", 'query': query},
                    function (response) {
                        bkg.console.log("Received from post");
                        bkg.console.log(response);
                        var r = JSON.parse(response.answer);
                        for (let step = 0; step < 5; step++) {
                            var link = document.getElementById("article_" + (step + 1));
                            console.log(bkg.console.log("article_" + (step + 1)));
                            console.log(link);
                            link.href = "https://en.wikipedia.org/wiki?curid=" + r.articles[step];
                            link.textContent = r.articles[step];
                            bkg.console.log(r.articles[step])
                        }
                        // log of popup - inspect popup
                        //https://en.wikipedia.org/wiki?curid=
                    });
            }
        });
    });
};