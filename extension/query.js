function callback(response) {
    console.log(response)
}

(function () {
    var queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    var query = urlParams.get('query');

    var xhr = new XMLHttpRequest();

    xhr.open("POST", "http://0.0.0.0:8080/gettop?query=" + query, true);
    xhr.onreadystatechange = function () { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            console.log("Recevied from the server");
            console.log(xhr.response);
            var resp = xhr.responseText;
            callback(resp);
        }
    };
    xhr.send();
}());

