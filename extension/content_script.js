(function () {
    let resizeIframe = function () {
        let iframe = document.getElementById('cool-id-iframe');
        iframe.height = iframe.contentWindow.document.body.scrollHeight + "px";
    };
    let getSelectionText = function () {
        let text = "";
        if (window.getSelection) {
            text = window.getSelection().toString();
        } else if (document.selection && document.selection.type != "Control") {
            text = document.selection.createRange().text;
        }
        return text;
    };

    let markSelection = (function () {
        let markerTextChar = "\ufeff";
        let markerTextCharEntity = "&#xfeff;";

        let markerEl, markerId = "sel_" + new Date().getTime() + "_" + Math.random().toString().substr(2);

        let selectionEl;

        return function (win) {
            win = win || window;
            let doc = win.document;
            let sel, range;
            // Branch for IE <= 8
            if (doc.selection && doc.selection.createRange) {
                // Clone the TextRange and collapse
                range = doc.selection.createRange().duplicate();
                range.collapse(false);

                // Create the marker element containing a single invisible character by creating literal HTML and insert it
                range.pasteHTML('<span id="' + markerId + '" style="position: relative;">' + markerTextCharEntity + '</span>');
                markerEl = doc.getElementById(markerId);
            } else if (win.getSelection) {
                sel = win.getSelection();
                range = sel.getRangeAt(0).cloneRange();
                range.collapse(false);

                // Create the marker element containing a single invisible character using DOM methods and insert it
                markerEl = doc.createElement("span");
                markerEl.id = markerId;
                markerEl.appendChild(doc.createTextNode(markerTextChar));
                range.insertNode(markerEl);
            }

            if (markerEl) {
                // Lazily create element to be placed next to the selection
                if (!selectionEl) {
                    selectionEl = doc.createElement("div");
                    selectionEl.style.border = "solid darkblue 1px";
                    selectionEl.style.backgroundColor = "#ffffff";
                    // selectionEl.innerHTML = "<iframe src='http://localhost:8080/?query='" + window.document.getSelection() + "></iframe>";
                    console.log(getSelectionText());
                    chrome.runtime.sendMessage({
                        query: getSelectionText(),
                        message: "IFRAME"
                    }, function (response) {
                        let el = doc.getElementById('cool-id-iframe');
                        console.log(response);
                        el.contentWindow.document.open();
                        el.contentWindow.document.write(response.answer);
                        el.document.close();
                    });
                    let iframe = doc.createElement('iframe');
                    // iframe.src = chrome.extension.getURL('iframe.html');
                    iframe.id = 'cool-id-iframe';
                    // iframe.src = "about:blank";
                    iframe.onload = "resizeIframe";
                    iframe.height = "400";
                    iframe.onmouseout = function () {
                        let div = window.document.getElementById('cool-extension');
                        if (div != null) {
                            div.parentNode.removeChild(div);
                            selectionEl = null;
                            if (window.getSelection) {
                                if (window.getSelection().empty) {  // Chrome
                                    window.getSelection().empty();
                                } else if (window.getSelection().removeAllRanges) {  // Firefox
                                    window.getSelection().removeAllRanges();
                                }
                            } else if (document.selection) {  // IE?
                                document.selection.empty();
                            }
                        }
                    };
                    selectionEl.appendChild(iframe);
                    selectionEl.style.position = "absolute";
                    selectionEl.id = 'cool-extension';

                    doc.body.appendChild(selectionEl);
                }

                // Find markerEl position http://www.quirksmode.org/js/findpos.html
                let obj = markerEl;
                let left = 0, top = 0;
                do {
                    left += obj.offsetLeft;
                    top += obj.offsetTop;
                } while (obj = obj.offsetParent);

                // Move the button into place.
                // Substitute your jQuery stuff in here
                selectionEl.style.left = left + "px";
                selectionEl.style.top = top + "px";

                markerEl.parentNode.removeChild(markerEl);
            }
        };
    })();

    function change() {
        if (getSelectionText() != '') {
            markSelection();
        }
        // if (window.document.getSelection() != '') {
        //     let div = window.document.createElement("div");
        //     div.innerHTML = "<iframe onload='resizeIframe(this)' src='http://localhost:8080/?query='" + window.document.getSelection() + "></iframe>";
        //     div.id = 'cool-extension';
        //     div.setAttribute("position", "absolute");
        //     div.setAttribute("top", "0");
        //     window.document.body.appendChild(div)
        // }
    }

    window.document.body.onmouseover = function () {
        timer = setTimeout(change, 3000);
    };

    window.document.body.onmouseout = function () {
        clearTimeout(timer);
    };

    let eventMethod = window.addEventListener
        ? "addEventListener"
        : "attachEvent";
    let eventer = window[eventMethod];
    let messageEvent = eventMethod === "attachEvent"
        ? "onmessage"
        : "message";

    eventer(messageEvent, function (e) {
        console.log(e);
        if (e.data['type'] === 'iframe') {
            console.log(e.data);
            chrome.runtime.sendMessage({
                text: e.data['text'],
                doc_id: e.data['doc_id'],
                par_n: e.data['par_n'],
                message: "CLICK"
            }, function (response) {
                console.log(response);
            })
        }

    });
    chrome.storage.local.get(["url", "par_n"], function (items) {
            console.log(items);
            if (items["url"] === document.location.href) {
                if (items["par_n"] === "-1") {
                    let tag = document.getElementById("firstHeading");
                    console.log(tag);
                    tag.style.backgroundColor = 'yellow';
                    tag.onmouseover = function () {
                        this.style.backgroundColor = 'white';
                    }
                } else {
                    let body = document.getElementsByClassName("mw-body")[0];
                    let tags = body.getElementsByTagName("p");
                    let ar = [].slice.call(tags);
                    ar = ar.filter(tag => !tag.className.includes('mw-empty-elt'));
                    let tag = ar[items["par_n"]];
                    console.log(tag);
                    tag.style.backgroundColor = 'yellow';
                    tag.onmouseover = function () {
                        this.style.backgroundColor = 'white';
                    }
                }


            }
            chrome.storage.local.set({"url": '', "par_n": 0}, function () {
            });
        }
    );
}());


