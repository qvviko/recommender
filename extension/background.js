// function that goes to remote web server. Server is specified in manifest
var check = function (query, callback) {
    var xhr = new XMLHttpRequest();

    // tune for you
    xhr.open("GET", "http://sprotasov.ru/data/dsa.txt?" + query, true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            var resp = xhr.responseText;
            console.log(resp);
            callback(resp);
        }
    };
    xhr.send();
};

var process_query = function (query, callback) {
    var xhr = new XMLHttpRequest();

    xhr.open("POST", "http://0.0.0.0:8080/gettop?query=" + query, true);
    xhr.onreadystatechange = function () { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            console.log("Recevied from the server");
            console.log(xhr.response);
            var resp = xhr.responseText;
            callback(resp);
        }
    };
    xhr.send();
};

var get_html = function (query, callback) {
    var xhr = new XMLHttpRequest();
    console.log(query);
    xhr.open("GET", "http://0.0.0.0:8080/?query=" + query, true);
    xhr.onreadystatechange = function () { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            callback(xhr.response);

        }
    };
    xhr.send();
};

// just to follow the tutorial
chrome.runtime.onInstalled.addListener(function () {
    // adds popup (maybe not needed?)
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
        chrome.declarativeContent.onPageChanged.addRules([{
            conditions: [
                new chrome.declarativeContent.PageStateMatcher({
                    pageUrl: {schemes: ['http', 'https']},
                })
            ],
            actions: [new chrome.declarativeContent.ShowPageAction()]
        }]);
    });


});

function getClickHandler() {
    return function (info, tab) {

        // The srcUrl property is only available for image elements.
        var url = 'http://localhost:8080/?query=' + info.selectionText;

        // Create a new window to the info page.
        chrome.windows.create({url: url, width: 520, height: 660});
        console.log(tab);
    };
}

chrome.contextMenus.create({
    "title": "Get articles",
    "type": "normal",
    "contexts": ['selection'],
    "onclick": getClickHandler()
});


// chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
//     chrome.tabs.query({active: true, lastFocusedWindow: true}, function (tabs) {
//         // query the active tab, which will be only one tab
//         //and inject the script in it
//         chrome.tabs.executeScript(tabs[0].id, {file: "content_script.js"});
//     });
// });
// var port = chrome.runtime.connect();

// chrome.tabs.onCreated.addListener(function (tabId, changeInfo, tab) {
//     chrome.tabs.query({active: true, lastFocusedWindow: true}, function (tabs) {
//         // query the active tab, which will be only one tab
//         //and inject the script in it
//         chrome.tabs.executeScript(tabs[0].id, {file: "content_script.js"});
//     });
// });
// accepts messages from frontend and responds with resp

chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        console.log(sender.tab ?
            "from a content script:" + sender.tab.url :
            "from the extension");
        // if this request is for us
        if (request.message === "REQ_articles") {
            process_query(request.query, function (resp) {
                console.log("Sending response to popup");
                sendResponse({answer: resp});
            });
        } else if (request.message === "IFRAME") {
            console.log("IFRAME");
            console.log(request);
            console.log(request.query);
            get_html(request.query, function (resp) {
                sendResponse({answer: resp})
            })
        } else if (request.message === "CLICK") {
            console.log("CLICK");
            console.log(request);
            var xhr = new XMLHttpRequest();
            let doc_id = request.doc_id.split('?curid=')[1] + '_' + request.par_n;
            var req = "http://0.0.0.0:8080/click?query=" + request.text + '&doc_id=' + doc_id;
            console.log(req);
            xhr.open("POST", req, true);
            xhr.onreadystatechange = function () { // Call a function when the state changes.
                if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                    console.log("Recevied from the server");
                    console.log(xhr.response);
                }
            };
            xhr.send();
            chrome.storage.local.set({"url": request.doc_id, "par_n": request.par_n}, function () {
            });

            chrome.storage.local.get(["url", "par_n"], function (items) {
                    console.log(items)
                }
            );

            sendResponse(true);
        }
        return true;
    });
// chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
//     alert("message received");
// });

// document.addEventListener("hello", function (data) {
//     chrome.runtime.sendMessage("test");
// })
