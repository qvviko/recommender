# Recommendations based on highlighted text

This repository and project has the following structure:
1. Directory `extension` contains everything related to the extensions part of the project
2. Directory `server` contains Flask web server for extension to send information to
3. Directory `notebooks` contains scripts and findings used throughout the research process

Please note that you wouldn't be able to use this project unless you build an index (I couldn't download it here as it too big)