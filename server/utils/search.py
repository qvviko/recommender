import sys
import collections
from abc import abstractmethod

import faiss
import numpy as np

sys.path.append("..")

from models import Paragraph
from sklearn.metrics.pairwise import cosine_similarity

Result = collections.namedtuple('Result', ['id', 'paragraph_id', 'distance'])


# PARAMS:
# UNKS_S - COEF
# CATEGORIES - BOOSTING COEF, RELEVANT THRESHOLD
# PARAGRAPH_N - BOOSTING COEF

class SearchException(RuntimeError):
    """Raised when error has occurred during searching"""


class Search:
    def __init__(self, model, common_index, id2docid, db_conn=None, docid2document_cats=None,
                 category_index=None, categoryid2cat=None):
        self.model = model
        self.common_index = common_index
        faiss.downcast_index(self.common_index.index).make_direct_map()
        self.id2docid = id2docid

        self.db_conn = db_conn

        self.docid2document_cats = docid2document_cats
        self.category_index = category_index
        self.categoryid2cat = categoryid2cat

        self.index_s = None
        self.unks_s = None
        self.category_b = None
        self.paragraph_num_b = None

    def update_parts(self, index_s=None, unks_s=None, category_b=None, paragraph_num_b=None):
        if index_s is not None:
            self.index_s = index_s.attach(self)
        if unks_s is not None:
            self.unks_s = unks_s.attach(self)
        if category_b is not None:
            self.category_b = category_b.attach(self)
        if paragraph_num_b is not None:
            self.paragraph_num_b = paragraph_num_b.attach(self)

    def search(self, query, n):
        # Basic index part
        if self.index_s is None:
            raise SearchException("Main index search hasn't been set!")

        embedded_query = np.array(self.model.encode([query]))
        res = self.index_s(embedded_query)

        # Unks index part
        if self.unks_s is not None:
            bert = self.model._first_module()
            unks = [word for word in query.split() if word not in bert.tokenizer.vocab]
            if unks:
                embedded_unks = self.model.encode(unks)
                seen = [doc.id for doc in res]
                for unk, emb_unk in zip(unks, embedded_unks):
                    unk_res = self.unks_s(embedded_query, np.expand_dims(emb_unk, axis=0), unk, seen)
                    res.extend(unk_res)
                    seen.extend([doc.id for doc in unk_res])

        # Boosting part

        # Categories
        if self.category_b is not None:
            self.category_b.boost(res, embedded_query)

        # Paragraphs
        if self.paragraph_num_b is not None:
            self.paragraph_num_b.boost(res)
        return sorted(res, key=lambda x: x.distance)[:n]


class DoubleSearch:
    def __init__(self, coef, search_1, search_2):
        self.coef = coef
        self.search_1 = search_1
        self.search_2 = search_2

    def search(self, query, n):
        res_1 = self.search_1.search(query, n)

        count = 0
        for r in res_1:
            if r.distance > self.coef:
                count += 1
        if count > len(res_1) / 2:
            res_2 = self.search_2.search(query, n)
            res_1.extend(res_2)
        return sorted(res_1, key=lambda x: x.distance)[:n]


class Part:
    def __init__(self):
        self.main_search: Search or 'None' = None

    def attach(self, main_search: Search):
        self.main_search: Search = main_search
        return self


class SearchPart(Part):
    def __init__(self, n):
        super().__init__()
        self.n = n

    @abstractmethod
    def search(self, *args, **kwargs):
        pass

    def __call__(self, *args, **kwargs):
        return self.search(*args, **kwargs)


class IndexSearch(SearchPart):

    def search(self, query, n=None):
        # Returns articles for the query
        number_to_find = self.n if n is None else n
        results = self.main_search.common_index.search(query, number_to_find)
        to_return = []
        for dists, ids in zip(*results):
            for dist, doc_id in zip(dists, ids):
                to_return.append(Result(*self.main_search.id2docid[doc_id], dist))
        return to_return


class UnksSearch(SearchPart):
    def __init__(self, n, unk_coef):
        super().__init__(n)
        self.coef = unk_coef

    def __check_word_in_text(self, word, doc_id):
        session = self.main_search.db_conn()
        q = session.query(Paragraph.text).filter(Paragraph.index_id == doc_id)
        for i in q.all():
            if word in i:
                session.close()
                return True
        session.close()
        return False

    def __calculate_dist(self, query, article_id):
        article_emb = self.main_search.common_index.reconstruct(int(article_id))
        return 2 - 2 * cosine_similarity(query, article_emb.reshape(1, -1))[0][0]

    def search(self, query, unk_emb, unk, seen, n=None):
        # Returns articles for unknown word
        number_to_find = self.n if n is None else n
        results = self.main_search.common_index.search(unk_emb, number_to_find)
        to_return = []
        for dists, ids in zip(*results):
            for dist, doc_id in zip(dists, ids):
                real_doc_id, paragraph_n = self.main_search.id2docid[doc_id]
                real_doc_id, paragraph_n = int(real_doc_id), int(paragraph_n)
                if real_doc_id not in seen and self.__check_word_in_text(unk, real_doc_id):
                    new_dist = self.__calculate_dist(query, doc_id)
                    to_return.append(Result(real_doc_id, paragraph_n, new_dist * self.coef))
        return to_return

    def attach(self, main_search: Search):
        if main_search.db_conn is None:
            raise SearchException('db_conn should be inside the main search')
        self.main_search: Search = main_search
        return self


class CategoryBoost(Part):
    def __init__(self, n, intersection_thresh, boost_coef):
        super().__init__()
        self.n = n
        self.intersection_thresh = intersection_thresh
        self.coef = boost_coef

    def boost(self, results, query, n=None):
        res = set(self.search(query, n))
        for i, article in enumerate(results):
            categories = self.main_search.docid2document_cats[str(article.id)]
            intersect_num = len(res & set(categories))
            results[i] = Result(article.id, article.paragraph_id, intersect_num * self.coef + article.distance)

    def search(self, query, n=None):
        # Returns intersected category ids
        number_to_find = self.n if n is None else n
        results = self.main_search.category_index.search(query, number_to_find)
        to_return = []
        for dists, ids in zip(*results):
            for dist, doc_id in zip(dists, ids):
                if dist < self.intersection_thresh:
                    to_return.append(self.main_search.categoryid2cat[str(doc_id)])
        return to_return

    def attach(self, main_search: Search):
        if main_search.docid2document_cats is None:
            raise SearchException('docid2document_cats should be inside the main search')
        if main_search.category_index is None:
            raise SearchException('category_index should be inside the main search')
        if main_search.categoryid2cat is None:
            raise SearchException('categoryid2cat should be inside the main search')

        self.main_search: Search = main_search
        return self


class ParagraphNumBoost(Part):
    def __init__(self, boost_coef):
        super().__init__()
        self.coef = boost_coef

    def boost(self, results):
        for i, article in enumerate(results):
            results[i] = Result(article.id, article.paragraph_id, self.coef(article.paragraph_id) + article.distance)
