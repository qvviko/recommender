import json
import os
from collections import defaultdict
import numpy as np

# '../../notebooks/recommend_tests/ground_truth.json'
relevance_dict = json.load(open(os.environ.get('GROUND_TRUTH_LOCATION')))
rel_dict = defaultdict(list)
base_url = "https://en.wikipedia.org/wiki?curid="
for key in relevance_dict.keys():
    for item in relevance_dict[key]:
        rel_dict[key].append(base_url + item['id'])


def dcg_at_k(relevance, k=10):
    power = np.power(2, relevance) - 1
    power = power / np.log2(np.array(list(range(k))) + 2)
    return sum(power[:k])


def pfound(relevance, prel=None, pbreak=0.15):
    # Prob to look at first item is 1
    if prel is None:
        prel = {0: 0, 1: 0.4}
    run = [1]
    for i in range(1, len(relevance)):
        run.append(run[i - 1] * (1 - prel[relevance[i - 1]]) * (1 - pbreak))
    for i in range(len(run)):
        run[i] *= prel[relevance[i]]
    return sum(run)


def evaluate(df):
    for i, row in df.iterrows():
        for j in range(1, 6):
            if row[f'top{j}'] in rel_dict[row.Query]:
                df.at[i, f'top{j}_rel'] = 1
            else:
                df.at[i, f'top{j}_rel'] = 0
    df_rel = df[['top1_rel', 'top2_rel', 'top3_rel', 'top4_rel', 'top5_rel']]
    df['dcg'] = [dcg_at_k(df_rel.iloc[i], 5) / 2.948459 for i in range(len(df_rel))]
    df['pfound'] = [pfound(list(df_rel.iloc[i])) for i in range(len(df_rel))]
    return df
