import json
import os
from collections import defaultdict

import faiss
import requests
from flask import Flask, request, make_response, render_template
from flask.json import jsonify
import numpy as np

from flask_cors import CORS, cross_origin
from sentence_transformers import SentenceTransformer
from bs4 import BeautifulSoup

# models
from models import *
from utils.search import Search, IndexSearch, UnksSearch, CategoryBoost


def check_word_in(doc_id, word):
    session = Session()
    q = session.query(Paragraph.text).filter(Paragraph.index_id == doc_id)
    for i in q.all():
        if word in i:
            session.close()
            return True
    session.close()
    return False


app = Flask(__name__)
CORS(app)


@app.route('/gettop', methods=['POST'])
@cross_origin()
def get_model_results():
    message = request.args
    print(message)
    if 'query' not in message.keys():
        print("NO QUERY")
        resp = make_response("No query", 400)
        return resp

    top5 = search_eng.search(message['query'], 5)
    print(top5)
    return jsonify({'articles': top5})


@app.route('/click', methods=['POST'])
@cross_origin()
def save_clicks():
    message = request.args
    print(message)
    if 'query' not in message.keys() and 'doc_id' not in message.keys():
        print("NO QUERY or DOC_ID")
        resp = make_response("No query or doc_id", 400)
        return resp
    session = Session()
    new_click = Click(message['query'], message['doc_id'])
    session.add(new_click)
    session.commit()
    session.close()
    return make_response("Done", 201)


@app.route('/', methods=['GET'])
@cross_origin()
def index():
    message = request.args
    print(message)
    if 'query' not in message.keys():
        print("NO QUERY")
        resp = make_response("No query", 400)
        return resp

    top5 = search_eng.search(message['query'], 5)
    print(top5)

    def_link = "https://en.wikipedia.org/wiki?curid="
    documents = []
    for article in top5:
        doc = {}
        url = def_link + str(article.id)
        ans = requests.get(url)  # TODO: change this, too slow
        soup = BeautifulSoup(ans.content, 'html.parser')
        doc['title'] = soup.find('h1', {'id': 'firstHeading'}).text
        for p in soup.findAll('p'):
            if len(p.text) > 3:
                doc['init_text'] = p.text
                break
        doc['url'] = url
        doc['par_n'] = str(article.paragraph_id)
        try:
            doc['img'] = 'https:' + soup.find('img')['src']
        except Exception:
            pass
        documents.append(doc)
    return render_template('search.html', query=request.args['query'], documents=documents)


if __name__ == '__main__':
    model = SentenceTransformer('bert-base-nli-mean-tokens')
    print("LOADED MODEL")

    faiss_index = faiss.read_index(os.environ.get('INDEX_LOCATION'))
    print("LOADED INDEX")

    keys_f = open(os.environ.get('KEYS_LOCATION'), 'rb')
    read = keys_f.read()
    keys = np.frombuffer(read, dtype=np.int64)
    keys = keys.reshape(-1, 2)
    docs_keys = {}
    for i in range(len(keys)):
        docs_keys[i] = (keys[i][0], keys[i][1])
    print("LOADED KEYS")

    category_index = faiss.read_index(os.environ.get('CATEGORY_INDEX_LOCATION'))
    print("LOADED CATEGORIES")
    id2cats = defaultdict(list, json.load(open(os.environ.get('CATEGORY_DOCS'))))
    print("LOADED ID2CATS")
    id2cat = json.load(open(os.environ.get('CATEGORY_KEYS')))
    print("LOADED ID2CAT")

    search_eng = Search(model,
                        faiss_index,
                        docs_keys,
                        Session,
                        id2cats,
                        category_index,
                        id2cat
                        )
    search_eng.update_parts(IndexSearch(100),
                            UnksSearch(100, float(os.environ.get('UNK_COEF'))),
                            CategoryBoost(100, float(os.environ.get('CATEGORY_THRESHOLD')),
                                          float(os.environ.get('CATEGORY_COEF'))),
                            )
    app.run(host='0.0.0.0', port=8080, debug=True, use_reloader=False)
