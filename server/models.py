import os

import sqlalchemy as db
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine = db.create_engine(os.environ.get("DB_DECLARATION"))
Base = declarative_base()
Session = sessionmaker(bind=engine)


class Click(Base):
    __tablename__ = 'Click'
    id = db.Column(db.Integer, primary_key=True)
    query = db.Column(db.String)
    doc = db.Column(db.String)

    def __init__(self, query, doc):
        self.query = query
        self.doc = doc

    def __repr__(self):
        return f"<Click(query={self.query}, doc={self.doc})>"


class Paragraph(Base):
    __tablename__ = 'Paragraph'
    id = db.Column(db.Integer, primary_key=True)
    index_id = db.Column(db.Integer)
    paragraph_id = db.Column(db.Integer)
    text = db.Column(db.String)

    def __init__(self, index_id, text, paragraph_id):
        self.index_id = index_id
        self.text = text
        self.paragraph_id = paragraph_id

    def __repr__(self):
        return f"<Paragraph(index_id={self.index_id}, text={self.text}, paragraph_id={self.paragraph_id})>"


Base.metadata.create_all(engine)
