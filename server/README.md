# Server
A simple server that uses trained doc2vec model to return top 10 matched wikipedia articles

Usage:
1. Install [Docker](https://www.docker.com/) Community Edition
2. Install [git](https://git-scm.com/)
3. [Clone project](https://git-scm.com/docs/git-clone)
4. Change environment according to your setup using `.env.template` 
5. Run project in docker-environment - `docker-compose up`, wait when project starts
([see more about docker-compose](https://docs.docker.com/compose/))
6. Do a POST request with {"query":your_query} params to website: [http://localhost:8080/gettop](http://localhost:8080/gettop) (address may changed, depends on environment)
6. OR add the browser extension to your chrome browser and do everything from there