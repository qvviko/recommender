# Notebooks
Here are some explanation of every directory
1. `index_creation` - here is every notebook describes index creation process (scripts and tryout)
2. `recommend_test` - notebooks containing tests and analysis for the recommendations given by the system and 
3. `Science!` - notebooks where I try some interesting ideas of different tools
4. `trainings` - notebooks containing training of some models
5. `tryouts` - notebooks with simple tryouts of different tools