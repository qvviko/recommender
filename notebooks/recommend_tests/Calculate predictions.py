#!/usr/bin/env python
# coding: utf-8

# In[3]:


import pandas as pd
import numpy as np
import faiss


# In[4]:


df = pd.read_csv("Querries.v2.csv")


# In

index = faiss.read_index("../index_creation/index_add4.bin")


# In[7]:


from sentence_transformers import SentenceTransformer
model = SentenceTransformer('bert-base-nli-mean-tokens', device='cuda:0')


# In[ ]:

# In[8]:


embedded_queries = model.encode(df.Query)


# In[9]:


embedded_queries = np.array(embedded_queries)


# In[ ]:


dist, answ = index.search(embedded_queries, 5)


# In[ ]:


dist.T.shape


# In[117]:


for i, (d, a) in enumerate(zip(dist.T, answ.T)):
    df[f'top{i+1}_dist'] = d
    df[f'top{i+1}'] = [f"https://en.wikipedia.org/wiki?curid={docs_keys[a1][0]}" for a1 in a]
    df[f'top{i+1}'] = None


# In[118]:


df


# In[119]:


df.to_csv('queries_res.csv')



