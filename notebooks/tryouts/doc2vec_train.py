# %%
import json
import multiprocessing
import os

from gensim.models.doc2vec import TaggedDocument, Doc2Vec


# %%


def read_file(file):
    f = open(os.path.join(load_dir, listdir[-1]))
    text = f.read()
    l = []
    for articles in text.split("\n"):
        if articles:
            l.append(json.loads(articles))
    return l


# %%

load_dir = "./input"
listdir = os.listdir(load_dir)
articles = []
for file in listdir:
    articles += read_file(file)


# %%
class TaggedWikiDocument(object):
    def __init__(self, wiki):
        self.wiki = wiki

    def __iter__(self):
        for article in self.wiki:
            yield TaggedDocument(article['text'], [article['title']])


# %%
documents = TaggedWikiDocument(articles)

cores = multiprocessing.cpu_count()
model = Doc2Vec(dm=0, dbow_words=1, vector_size=200, window=8, min_count=19, epochs=10, workers=cores)
# %%
model.build_vocab(documents)
print(str(model))

from gensim.models.callbacks import CallbackAny2Vec


class EpochSaver(CallbackAny2Vec):
    '''Callback to save model after each epoch.'''

    def __init__(self):
        self.path_prefix = "doc2vec"
        self.epoch = 0

    def on_epoch_begin(self, model):
        print("Epoch #{} start".format(self.epoch))

    def on_epoch_end(self, model):
        print("Epoch #{} end".format(self.epoch))
        output_path = '{}_epoch{}.model'.format(self.path_prefix, self.epoch)
        model.save(output_path)
        self.epoch += 1


# %%
epoch_saver = EpochSaver()
model.train(documents, total_examples=model.corpus_count, epochs=10, callbacks=[epoch_saver])

# %%
vector = model.infer_vector(["Machine", "learning"])

# %%

model.docvecs.most_similar([vector])
