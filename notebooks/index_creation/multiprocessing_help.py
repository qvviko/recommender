import json
def init(l, model, out):
    global lock
    global model_l
    global out_file2
    out_file2 = out
    lock = l
    model_l = model
    
def save_article(articles):
    print(len(articles))
    for article in articles:
        d = {'title':article['title'], 'url':article['url'], 'paragraphs':[]}
        text  = article['text']
        sentences = text.split("\n\n")
        sentences = list(filter(bool, sentences))[1:]

        names = []
        texts = []
        for i, sentence in enumerate(sentences):
            if len(sentence) < 10:
                continue
            names.append(f"{article['title']}_{i}")
            texts.append(sentence)
        embedded = model_l.encode(texts)
        for i in range(len(names)):
            embedded[i].dtype = float
            paragraph = {"name":names[i], "embedding":list(embedded[i])}
            d['paragraphs'].append(paragraph)
        lock.acquire()
        out_file = open(out_file2, "a+")
        json.dump(d, out_file)
        out_file.write("\n")
        out_file.flush()
        lock.release()
    del article
